<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 21/10/2017
 * Time: 20:03
 */

namespace App\lib\Factory;


use Phalcon\Di\FactoryDefault;

interface FactoryInterface
{

    public function createInstance(FactoryDefault $di);

}