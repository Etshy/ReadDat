<?php
namespace App\Security\Mail;

use Phalcon\Config;
use Phalcon\Mvc\User\Component;
use Phalcon\Mvc\View;

/**
 * App\Security\Mail\Mail
 * Sends e-mails based on pre-defined templates
 */
class Mail extends Component
{
    protected $transport;

    /**
     * Applies a template to be used in the e-mail
     *
     * @param string $name
     * @param array $params
     * @return string
     */
    public function getTemplate($name, $params)
    {
        $parameters = array_merge([
            'publicUrl' => $this->config->application->publicUrl
        ], $params);

        return $this->view->getRender('emailTemplates', $name, $parameters, function ($view) {
            $view->setRenderLevel(View::LEVEL_LAYOUT);
        });

        return $view->getContent();
    }

    /**
     * Sends e-mails
     *
     * @param array $to
     * @param string $subject
     * @param string $name
     * @param array $params
     * @return bool|int
     * @throws Exception
     */
    public function send($to, $subject, $viewName, $params)
    {
        // Settings
        $mailConfig = (array) $this->config->mail;

        // view relative to the folder viewsDir (REQUIRED)
        $viewPath = 'emailTemplates/'.$viewName;

        $parameters = array_merge([
            'publicUrl' => $this->config->application->publicUrl
        ], $params);

        $mailer = new \Phalcon\Ext\Mailer\Manager($mailConfig);

        $message = $mailer->createMessageFromView($viewPath, $parameters)
            ->to($to)
            ->subject($subject);

        return $message->send();

    }
}
