<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 18/11/2017
 * Time: 22:59
 */

namespace App\Services;


use App\Models\BaseModel;
use App\Models\Chapter;
use App\Models\Teams;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model;

class TeamsService
{

    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     * @var Teams
     */
    protected $teamsModel;

    /**
     * TeamsService constructor.
     */
    public function __construct(FactoryDefault $di, Teams $teamsModel)
    {
        $this->di = $di;
        $this->teamsModel = $teamsModel;
    }

    /**
     * Save the given teams
     *
     * @param Teams $teams
     * @return bool
     *
     */
    public function saveTeams(Teams $teams) {
        if($teams->getId()) {
            return $teams->update();
        } else {
            return $teams->create();
        }
    }

    /**
     * Get teams by given id
     *
     * @param $id
     * @return Teams
     */
    public function getTeamsById($id)
    {
        return $this->teamsModel->findFirst($id);
    }

    /**
     * @param array $params
     * @return Model\ResultsetInterface
     */
    public function getTeamsByParam(array $params = array())
    {
        //TODO implements new find, using querybuilder.
        //Default way to pass params is shit
        $qb = $this->teamsModel::query();
        $qb->where('deleted = false');

        //Add params

        return $qb->execute();
    }

}