<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 13/11/2017
 * Time: 01:40
 */

namespace App\Services;


use App\Exceptions\ChapterPageException;
use Phalcon\Di\FactoryDefault;
use Phalcon\Http\Request\File;

class ImageService
{

    const SERIES_IMAGE_PATH = '/series_images';
    const CHAPTER_PAGES_PATH = '/comics';

    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     * ImageService constructor.
     */
    public function __construct(FactoryDefault $di)
    {
        $this->di = $di;
    }


    /**
     * Handle the series image
     * @param $files
     * @return array
     */
    public function handleSeriesImage($files)
    {
        $config = $this->di->get('config');
        $baseUri = $config->application->baseFileUri.self::SERIES_IMAGE_PATH;
        $basePath = BASE_PATH . '/public' . $baseUri;

        if(!file_exists($basePath)) {
            mkdir($basePath);
        }
        $paths = [];
        foreach($files as $upload){
            $endPath = '/'.$upload->getname();
            $path = $basePath.$endPath;
            $upload->moveTo($path);
            $paths[]=$baseUri.$endPath;
        }
        return $paths;
    }

    /**
     * @param $files
     * @param int $seriesId
     * @param int $chapterId
     * @return array
     */
    public function moveUploadedChapPages(File $file, int $seriesId, int $chapterId)
    {
        $config = $this->di->get('config');
        $baseUri = $config->application->baseFileUri.self::CHAPTER_PAGES_PATH.'/'.$seriesId.'/'.$chapterId;
        $basePath = BASE_PATH . '/public' . $baseUri;

        if(!file_exists($basePath)) {
            mkdir($basePath, 0777, true);
        }

        $paths = [];
        //normally, only one file
        $endPath = '/'.$file->getname();
        $path = $basePath.$endPath;

        if(file_exists($path)) {
            throw new ChapterPageException('File already exists.');
        }

        $file->moveTo($path);
        $paths['urlPath']=$baseUri.$endPath;
        $paths['localPath'] = $path;

        return $paths;
    }
}