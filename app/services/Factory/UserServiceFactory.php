<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 21/10/2017
 * Time: 19:47
 */

namespace App\Services\Factory;


use App\lib\Factory\FactoryInterface;
use App\Models\Users;
use App\Services\UserService;
use Phalcon\Di\FactoryDefault;

class UserServiceFactory implements FactoryInterface
{
    public function __construct()
    {
        //Call createInstance to create the Service instance
    }

    /**
     * Create Instance
     *
     * Create the UserService Instance
     *
     * @param FactoryDefault $di
     * @return UserService
     */
    public function createInstance(FactoryDefault $di){
        //Dependency to add
        $userModel = new Users();

        return new UserService($di, $userModel);
    }

}