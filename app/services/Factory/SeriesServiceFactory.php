<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 05/11/2017
 * Time: 18:08
 */

namespace App\Services\Factory;


use App\lib\Factory\FactoryInterface;
use App\Models\Chapter;
use App\Models\Series;
use App\Services\SeriesService;
use Phalcon\Di\FactoryDefault;

class SeriesServiceFactory implements FactoryInterface
{

    public function __construct()
    {
        //Call createInstance to create the Service instance
    }

    /**
     * Create Instance
     *
     * Create the UserService Instance
     *
     * @param FactoryDefault $di
     * @return SeriesService
     */
    public function createInstance(FactoryDefault $di){
        //Dependency to add
        $seriesModel = new Series();
        $chapterModel = new Chapter();

        return new SeriesService($di, $seriesModel, $chapterModel);
    }

}