<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 30/11/2017
 * Time: 22:27
 */

namespace App\Services\Factory;


use App\lib\Factory\FactoryInterface;
use App\Models\Pages;
use App\Services\PagesService;
use Phalcon\Di\FactoryDefault;

class PagesServiceFactory implements FactoryInterface
{
    public function createInstance(FactoryDefault $di)
    {
        $imageService = $di->get('ImageService');
        $pagesModel = new Pages();
        return new PagesService($di, $imageService, $pagesModel);
    }

}