<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 12/11/2017
 * Time: 02:11
 */

namespace App\Services\Factory;


use App\lib\Factory\FactoryInterface;
use App\Services\ErrorService;
use Phalcon\Di\FactoryDefault;

/**
 * Class ErrorServiceFactory
 * @package App\Services\Factory
 */
class ErrorServiceFactory implements FactoryInterface
{

    /**
     * ErrorServiceFactory constructor.
     */
    public function __construct()
    {

    }

    public function createInstance(FactoryDefault $di)
    {
        //Dependency to add
        return new ErrorService($di);
    }
}