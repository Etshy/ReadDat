<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 18/11/2017
 * Time: 22:57
 */

namespace App\Services\Factory;


use App\lib\Factory\FactoryInterface;
use App\Models\Teams;
use App\Services\TeamsService;
use Phalcon\Di\FactoryDefault;

class TeamsServiceFactory implements FactoryInterface
{


    /**
     * TeamsServiceFactory constructor.
     */
    public function __construct()
    {
        //Call createInstance to create the Service instance
    }

    public function createInstance(FactoryDefault $di)
    {
        //Dependency to add
        $teamsModel = new Teams();

        return new TeamsService($di, $teamsModel);
    }

}