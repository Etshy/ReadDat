<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 13/11/2017
 * Time: 01:40
 */

namespace App\Services\Factory;


use App\lib\Factory\FactoryInterface;
use App\Services\ImageService;
use Phalcon\Di\FactoryDefault;

/**
 * Class ImageServiceFactory
 * @package App\Services\Factory
 */
class ImageServiceFactory implements FactoryInterface
{

    /**
     * ImageServiceFactory constructor.
     */
    public function __construct()
    {
    }


    public function createInstance(FactoryDefault $di)
    {
        return new ImageService($di);
    }

}