<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 21/10/2017
 * Time: 19:18
 */
namespace App\Services;

use App\Models\EmailConfirmations;
use App\Models\Users;
use Phalcon\Di\FactoryDefault;
use Phalcon\Di\Service;

class UserService
{
    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     *
     * @var Users
     */
    protected $usersModel;

    public function __construct($di, Users $userModel){
        $this->di = $di;
        $this->usersModel = $userModel;
        return $this;
    }

    /**
     * Save User
     *
     * @param Users $user
     * @param array|null $whitelist
     * @return bool
     */
    public function saveUser(Users $user, array $whitelist = null){
        if($user->getId()) {
            return $user->update();
        } else {
            return $user->create();
        }
    }

    /**
     * Get user by given id
     *
     * @param $id
     * @return \Phalcon\Mvc\Model
     */
    public function getUserById($id)
    {
        return $this->usersModel->findFirst($id);
    }

    /**
     * @param array|null $aParam
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getUsersByParam(array $aParam = null)
    {
        $qb = $this->usersModel::query();
        $qb->where('deleted = false');

        //Add params

        return $qb->execute();
    }

    public function sendConfirmationEmail(Users $user)
    {
        // Only send the confirmation email if emails are turned on in the config
        if ($this->di->get('config')->useMail) {
            if ($user->getActive() == false) {
                $emailConfirmation = new EmailConfirmations();
                $emailConfirmation->setUsersId($user->getId());
                if ($emailConfirmation->save()) {
                    $this->di
                        ->get('flashSession')
                        ->notice('A confirmation mail has been sent to ' . $user->getEmail());
                }
            }
        }
    }
}