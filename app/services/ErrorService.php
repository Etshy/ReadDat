<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 12/11/2017
 * Time: 02:13
 */

namespace App\Services;


use Phalcon\Di\FactoryDefault;

/**
 * Class ErrorService
 * @package App\Services
 */
class ErrorService
{

    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     * ErrorService constructor.
     */
    public function __construct(FactoryDefault $di)
    {
        $this->di = $di;
    }

    public function sendEmail(\Error $e)
    {
        // Only send the confirmation email if emails are turned on in the config
        if ($this->di->get('config')->useMail) {
            //TODO create ErrorMail class and send mail on aftersave
        }
    }
}