<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 05/11/2017
 * Time: 18:09
 */

namespace App\Services;


use App\Models\Chapter;
use App\Models\Series;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Mvc\Model\Resultset\Simple;

/**
 * Class SeriesService
 * @package App\Services
 */
class SeriesService
{

    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     *
     * @var Series
     */
    protected $seriesModel;

    /**
     * @var Chapter
     */
    protected $chapterModel;

    /**
     * SeriesService constructor.
     * @param $di
     * @param Series $seriesModel
     */
    public function __construct($di, Series $seriesModel, Chapter $chapterModel){
        $this->di = $di;
        $this->seriesModel = $seriesModel;
        $this->chapterModel = $chapterModel;
        return $this;
    }

    /**
     * Save the given series
     *
     * @param Series $series
     * @return bool
     *
     */
    public function saveSeries(Series $series) {
        if($series->getId()) {
            return $series->update();
        } else {
            return $series->create();
        }
    }

    /**
     * Save the given chapter
     * @param Chapter $chapter
     * @return bool
     */
    public function saveChapter(Chapter $chapter) {
        if($chapter->getId()) {
            return $chapter->update();
        } else {
            return $chapter->create();
        }
    }

    /**
     * Get series by given id
     *
     * @param $id
     * @return \Phalcon\Mvc\Model
     */
    public function getSeriesById($id)
    {
        return $this->seriesModel->findFirst($id);
    }

    /**
     * Get series by given slug
     *
     * @param $id
     * @return Series
     */
    public function getSeriesBySlug($slug)
    {
        return $this->seriesModel->findFirstBySlug($slug);
    }

    /**
     * Get Series by given params
     *
     * @param array $aParam
     * @return array
     */
    public function getSeriesByParam(array $params = array())
    {
        $qb = Series::query();
        $qb->where('deleted = false');
        //params

        $result = $qb->execute();

        return $qb->execute();
    }

    /**
     * Get chapter by id
     * @param $id
     * @return \Phalcon\Mvc\Model
     */
    public function getChapterById($id)
    {
        return $this->chapterModel->findFirst($id);
    }

    /**
     * Get chapter by params
     * @param array $params
     * @return Simple|Resultset
     */
    public function getChapterByParams(array $params = array())
    {
        $qb = Chapter::query();
        //$qb->where('deleted = false');
        //params
        if(array_key_exists('chapterNumber', $params)) {
            $qb->andWhere('number = :chapterNumber:');
        }
        if(array_key_exists('chapterSubNumber', $params)) {
            $qb->andWhere('subNumber = :chapterSubNumber:');
        }
        if(array_key_exists('seriesId', $params)) {
            $qb->andWhere('seriesId = :seriesId:');
        }

        $qb->orderBy('number');

        if(!empty($params)) {
            $qb->bind($params);
        }

        return $qb->execute();
    }


    public function getLastestChaptersForHomepage()
    {
        $qb = Chapter::query();
        $qb->where('visibility = true');
        $qb->where('deleted = false');

        $qb->orderBy('createdAt DESC');

        return $qb->createBuilder();
    }

    /**
     * Manual deletion because of Phalcon problems with ORM relationships
     * @param Chapter $chapter
     */
    public function deleteChapterTeamsRelationshipFromChapter(Chapter $chapter)
    {
        $sql = '
        DELETE 
        FROM chapter_teams
        WHERE chapterId = '.$chapter->getId();
        $chapter->getReadConnection()->query($sql);
    }
}