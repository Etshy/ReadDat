<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 30/11/2017
 * Time: 22:28
 */

namespace App\Services;


use App\Exceptions\ChapterPageException;
use App\Models\Pages;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Phalcon\Di\FactoryDefault;
use Phalcon\Http\Request\File;

class PagesService
{

    /**
     * @var FactoryDefault
     */
    protected $di;

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * @var Pages
     */
    protected $pagesModel;

    /**
     * PagesService constructor.
     */
    public function __construct(FactoryDefault $di, ImageService $imageService, Pages $pages)
    {
        $this->di = $di;
        $this->imageService = $imageService;
        $this->pagesModel = $pages;
    }

    /**
     * @param array $files
     * @param int $seriesId
     * @param int $chapterId
     * @param int $order
     * @return bool
     * @throws ChapterPageException
     */
    public function handleChapPageUpload(array $files, int $seriesId, int $chapterId, int $order)
    {
        //only one image per request
        $file = $files[0];
        /**
         * @var $file File
         */

        //basic File validation (wait for better implementation from Phalcon
        $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG);
        $detectedType = exif_imagetype($file->getTempName());
        $error = !in_array($detectedType, $allowedTypes);
        if ($error) {
            throw new ChapterPageException('File type not authorized');
        }

        $paths = $this->imageService->moveUploadedChapPages($file, $seriesId, $chapterId);

        $page = new Pages();
        $page->setChapterId($chapterId);
        $page->setPosition($order);
        $page->setFilename($file->getName());
        $page->setLocalPath($paths['localPath']);
        $page->setUrlPath($paths['urlPath']);
        $page->setMime($file->getType());

        $return = $this->savePage($page);
        if($return == true) {
            $id = $page->getReadConnection()->lastInsertId();
            $appConfig = $this->di->get('config')->application;

            $return = [
                'page_id' => $id,
                'delete_method' => 'DELETE',
                'delete_url' => $appConfig->publicUrl . '/admin/pages/delete/' . $id,
                'file_url' => $appConfig->publicUrl . $paths['urlPath'],
            ];
            return $return;
        }
        return $return;
    }

    /**
     * @param int $id
     * @return bool|\Phalcon\Mvc\Model\MessageInterface[]
     */
    public function deletePage(int $id)
    {
        $page = $this->getPageById($id);
        /**
         * @var $page Pages
         */
        //delete the local file
        unlink($page->getLocalPath());

        if ($page->delete()) {
            return true;
        }
        return $page->getMessages();
    }

    /**
     * Save the given chapter
     * @param Pages $page
     * @return bool
     */
    public function savePage(Pages $page) {
        if($page->getId()) {
            return $page->update();
        } else {
            return $page->create();
        }
    }

    /**
     * @param int $chapterId
     * @return \Phalcon\Mvc\Model\ResultsetInterface
     */
    public function getPagesByChapter(int $chapterId)
    {
        $qb = Pages::query();
        //params
        $param = [
            'chapterId' => $chapterId
        ];
        $qb->where('chapterId = :chapterId:');
        $qb->orderBy(' position ASC ');
        $qb->bind($param);

        return $qb->execute();
    }

    public function getPageById(int $id)
    {
        return $this->pagesModel::findFirst($id);
    }

    /**
     * @param array $data
     */
    public function updatePagesOrder(array $data)
    {
        $db = $this->di->get('db');

        /**
         * @var $db Mysql
         */

        $db->begin();

        foreach ($data as $file) {

            $page = $this->pagesModel::findFirstById($file['id']);
            /**
             * @var $page Pages
             */
            $page->setPosition($file['order']);
            $this->savePage($page);
        }

        $db->commit();
    }
}