<?php

$router = new Phalcon\Mvc\Router();

// Define your routes here
/*
$router->add(
    '/',
    [
        'namespace'  => 'App\Controllers',
        'controller' => 'Index',
        'action'     => 'index',
    ]
);
*/

$router->add(
    '/login',
    [
        'controller' => 'Session',
        'action'     => 'login',
    ]
);
$router->add(
    '/logout',
    [
        'controller' => 'Session',
        'action'     => 'logout',
    ]
);
$router->add(
    '/register',
    [
        'controller' => 'Session',
        'action'     => 'register',
    ]
);
$router->add(
    '/confirm/{code}/{email}',
    [
        'controller' => 'User_Control',
        'action' => 'confirmEmail'
    ]
);
// Matches loggedin route
$router->add(
    '/admin/:controller/([a-zA-Z0-9_-]*)/:params',
    [
        'namespace'  => 'App\Controllers\Admin',
        'controller' => 1,
        'action'     => 2,
        'params'     => 3,
    ]
)->convert('action', function($action) {
    return Phalcon\Text::camelize($action);
});

// Matches 'Reader' route

$router->add(
    '/reader/([a-z0-9\-]+)/([0-9]+)[/]{0,1}([0-9]+)*/page[/]{0,1}([0-9]+)*',
    [
        'namespace'  => 'App\Controllers',
        'controller' => 'Reader',
        'action'     => 'index',
        'seriesSlug'     => 1,
        'chapterNumber'  => 2,
        'chapterSubNumber' => 3,
        'pageNumber' => 4,
    ]
);
$router->add(
    '/reader/([a-z0-9\-]+)/([0-9]+)/page[/]{0,1}([0-9]+)*',
    [
        'namespace'  => 'App\Controllers',
        'controller' => 'Reader',
        'action'     => 'index',
        'seriesSlug'     => 1,
        'chapterNumber'  => 2,
        'pageNumber' => 3,
    ]
);
$router->add(
    '/reader/([a-z0-9\-]+)/([0-9]+)[/]{0,1}([0-9]+)*',
    [
        'namespace'  => 'App\Controllers',
        'controller' => 'Reader',
        'action'     => 'index',
        'seriesSlug'     => 1,
        'chapterNumber'  => 2,
        'chapterSubNumber' => 3
    ]
);

// Remove trailing slashes automatically
$router->removeExtraSlashes(true);
return $router;
