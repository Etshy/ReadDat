<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

$loader->registerNamespaces(
    [
        'App\Controllers' => $config->application->controllersDir,
        'App\Models' => $config->application->modelsDir,
        'App\Services' => $config->application->servicesDir,
        'App\Forms' => $config->application->formsDir,
        'App\Security' => $config->application->securityDir,
        'App\i18n' => $config->application->i18nDir,
        'App\Exceptions' => $config->application->exceptionsDir,

        'App\lib' => $config->application->libDir,
    ]
)->register();

// Use composer autoloader to load vendor classes

require_once BASE_PATH . '/vendor/autoload.php';
