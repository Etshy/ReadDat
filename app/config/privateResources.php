<?php

use Phalcon\Config;

/**
 * Defines the private routes. permissions check will occur for these controllers/actions
 */
return new Config([
    'privateResources' => [
        //List fo private resources
        'App\Controllers\Admin\SeriesController' => [
            'index', 'add', 'edit', 'addChapter', 'editChapter', 'deleteSeries'
        ],
        'App\Controllers\Admin\PagesController' => [
            'uploadPages', 'delete', 'updatePagesOrder'
        ],
        'App\Controllers\Admin\ProfileController' => [
            'index',
        ],
        'App\Controllers\Admin\TeamsController' => [
            'index', 'add', 'edit'
        ],
        'App\Controllers\Admin\UsersController' => [
            'index', 'details'
        ],
        /**

         'Controller' => [
            'action'
         ]

         */
    ]
]);
