<?php

use Phalcon\Crypt;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
    return include APP_PATH . "/config/config.php";
});

/**
 * Mongo connection
 */
/*
$di->set(
    'mongo',
    function () {
        $config = $this->getConfig();

        $params = [
            'host'     => $config->database->host,
            'username'     => $config->database->name,
            'password' => $config->database->password,
            'dbname'   => $config->database->dbname,
            'charset'  => $config->database->charset,
            'replicaSet' => $config->database->replicaSet,
        ];

        $mongo = new MongoClient(
            'mongodb://'+$params['username']+':'+$params['password']+'@'+$params['host']+'/'+$params['dbname']+'?ssl=true&replicaSet='+$params['replicaSet']+'&authSource=admin'
        );

        return $mongo->selectDB($params['dbname']);
    },
    true
);
*/
/**
 * Mongo CollectionManager
 */
/*
$di->set('collectionManager', function(){
    return new Phalcon\Mvc\Collection\Manager();
}, true);
*/
/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
    $config = $this->getConfig();
    $class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;

    $params = [
        'host'     => $config->database->host,
        'username'     => $config->database->name,
        'password' => $config->database->password,
        'dbname'   => $config->database->dbname,
        'charset'  => $config->database->charset
    ];

    if ($config->database->adapter == 'Postgresql') {
        unset($params['charset']);
    }

    return new $class($params);
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
    $config = $this->getConfig();

    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
});

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
    $config = $this->getConfig();

    $view = new View();
    $view->setDI($this);
    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines([
        '.volt' => function ($view) {
            $config = $this->getConfig();

            $volt = new VoltEngine($view, $this);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_'
            ]);

            return $volt;
        },
        '.phtml' => PhpEngine::class

    ]);

    return $view;
});

/**
 * Loading routes from the routes.php file
 */
$di->set('router', function () {
    return require APP_PATH . '/config/router.php';
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->setShared('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

/**
 * Flash service with custom CSS classes
 */
$di->set('flash', function () {
    return new Flash([
        'error' => 'ui error message',
        'success' => 'ui success message',
        'notice' => 'ui info message',
        'warning' => 'ui warning message'
    ]);
});

/**
 * Flash service with custom CSS classes
 */
$di->set('flashSession', function () {
    return new \Phalcon\Flash\Session([
        'error' => 'ui error message',
        'success' => 'ui success message',
        'notice' => 'ui info message',
        'warning' => 'ui warning message'
    ]);
});

/**
 * Dispatcher use a default namespace
 */
$di->set('dispatcher', function () {
    $dispatcher = new \Phalcon\Mvc\Dispatcher();
    $dispatcher->setDefaultNamespace('App\Controllers');
    return $dispatcher;
});

/**
 * Custom authentication component
 */
$di->set('auth', function () {
    return new \App\Security\Auth\Auth();
});

/**
 * Mail service uses AmazonSES
 */
$di->set('mail', function () {
    return new App\Security\Mail\Mail();
});

/**
 * Setup the private resources, if any, for performance optimization of the ACL.
 */
$di->setShared('AclResources', function() {
    $pr = [];
    if (is_readable(APP_PATH . '/config/privateResources.php')) {
        $pr = include APP_PATH . '/config/privateResources.php';
    }
    return $pr;
});

/**
 * Access Control List
 * Reads privateResource as an array from the config object.
 */
$di->set('acl', function () {
    $acl = new \App\Security\Acl\Acl();
    $pr = $this->getShared('AclResources')->privateResources->toArray();
    $acl->addPrivateResources($pr);
    return $acl;
});

/**
 * Crypt service
 */
$di->set('crypt', function () {
    $config = $this->getConfig();

    $crypt = new Crypt();
    $crypt->setKey($config->application->cryptSalt);
    return $crypt;
});

//My Services (service/logic class like we found them in ZF2/3 and Symfony2/3)
/**
 * @return \App\i18n\Locale
 */
$di->set('Locale', function() use ($di) {
    $LocaleFactory = new \App\i18n\Factory\LocaleFactory();
    $locale = $LocaleFactory->createInstance($di);
    return $locale;
});
/**
 * @return \App\Services\UserService
 */
$di->set('UserService', function() use ($di) {
    $userServiceFactory = new \App\Services\Factory\UserServiceFactory();
    $userService = $userServiceFactory->createInstance($di);
    return $userService;
});
/**
 * @return \App\Services\SeriesService
 */
$di->set('SeriesService', function() use ($di) {
    $seriesServiceFactory = new \App\Services\Factory\SeriesServiceFactory();
    $seriesService = $seriesServiceFactory->createInstance($di);
    return $seriesService;
});
/**
 * @return \App\Services\ErrorService
 */
$di->set('ErrorService', function() use ($di) {
    $errorServiceFactory = new \App\Services\Factory\ErrorServiceFactory();
    $errorService = $errorServiceFactory->createInstance($di);
    return $errorService;
});
/**
 * @return \App\Services\ImageService
 */
$di->set('ImageService', function() use ($di) {
    $imageServiceFactory = new \App\Services\Factory\ImageServiceFactory();
    $imageService = $imageServiceFactory->createInstance($di);
    return $imageService;
});
/**
 * @return \App\Services\TeamsService
 */
$di->set('TeamsService', function() use ($di) {
    $teamsServiceFactory = new \App\Services\Factory\TeamsServiceFactory();
    $teamsService = $teamsServiceFactory->createInstance($di);
    return $teamsService;
});
/**
 * @return \App\Services\PagesService
 */
$di->set('PagesService', function() use ($di) {
    $pagesServiceFactory = new \App\Services\Factory\PagesServiceFactory();
    $pageService = $pagesServiceFactory->createInstance($di);
    return $pageService;
});