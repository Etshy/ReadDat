<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 15/10/2017
 * Time: 00:20
 */

namespace App\Models;


use App\Models\Interfaces\ModelSoftDeletableInterface;
use App\Models\Traits\ModelSoftDeletableTrait;
use Phalcon\Mvc\Model\Behavior\Timestampable;
use Phalcon\Mvc\Model\Relation;

class Series extends BaseModel implements ModelSoftDeletableInterface
{
    use ModelSoftDeletableTrait;

    const READER_MODE_CLASSIC = 'classic';
    const READER_MODE_FLIPBOOK = 'flipbook';
    const READER_MODE_WEBTOON = 'webtoon';

    const READ_DIRECTION_LTR = 'ltr';
    const READ_DIRECTION_RTL = 'rtl';

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $additionalNames;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     * Path to an image
     */
    protected $image;

    /**
     * @var string
     */
    protected $author;

    /**
     * @var string
     */
    protected $artist;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var boolean
     */
    protected $adultWarning;

    /**
     * @var string
     */
    protected $readerMode;

    /**
     * @var string
     */
    protected $readDirection;

    /**
     * @var string
     */
    protected $visibility;

    /**
     * @var string
     */
    protected $customTitle;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAdditionnalNames()
    {
        return $this->additionnalNames;
    }

    /**
     * @param string $additionnalNames
     */
    public function setAdditionnalNames($additionnalNames)
    {
        $this->additionnalNames = $additionnalNames;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getArtists()
    {
        return $this->artists;
    }

    /**
     * @param string $artists
     */
    public function setArtists($artists)
    {
        $this->artists = $artists;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isAdultWarning()
    {
        return $this->adultWarning;
    }

    /**
     * @param bool $adultWarning
     */
    public function setAdultWarning($adultWarning)
    {
        $this->adultWarning = $adultWarning;
    }

    /**
     * @return string
     */
    public function getReaderMode()
    {
        return $this->readerMode;
    }

    /**
     * @param string $readerMode
     */
    public function setReaderMode($readerMode)
    {
        $this->readerMode = $readerMode;
    }

    /**
     * @return string
     */
    public function getReadDirection()
    {
        return $this->readDirection;
    }

    /**
     * @param string $readDirection
     */
    public function setReadDirection($readDirection)
    {
        $this->readDirection = $readDirection;
    }

    /**
     * @return string
     */
    public function getCustomTitle()
    {
        return $this->customTitle;
    }

    /**
     * @param string $customTitle
     */
    public function setCustomTitle($customTitle)
    {
        $this->customTitle = $customTitle;
    }

    /**
     * @return string
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * @param string $visibility
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }

    public function initialize()
    {
        /*
        $this->addBehavior(
            new Timestampable(
                [
                    'beforeValidationOnCreate' => [
                        'field'  => 'createdAt',
                        'format' => function () {
                            $datetime = new \DateTime();
                            return $datetime->format(\DateTime::ISO8601);
                        }
                    ],
                    'beforeValidationOnUpdate' => [
                        'field'  => 'updatedAt',
                        'format' => function () {
                            $datetime = new \DateTime();
                            return $datetime->format(\DateTime::ISO8601);
                        }
                    ]
                ]
            )
        );
        */

        //1 series has multiple chapter so chapter belongs to series
        $this->hasMany('id',
            Chapter::class,
            'seriesId',
            [
                "alias" => 'chapters',
                "foreignKey" => [
                    "action" => Relation::ACTION_CASCADE,
                ]
            ]);

        $this->keepSnapshots(true);
    }

    /**
     * Before create the series
     */
    public function beforeValidationOnCreate()
    {
        parent::beforeValidationOnCreate();
        if (empty($this->readerMode)) {
            //reader mode classic by default
            $this->readerMode = self::READER_MODE_CLASSIC;
        }
        if($this->readerMode == self::READER_MODE_FLIPBOOK && empty($this->readDirection))
        {
            $this->readDirection = self::READ_DIRECTION_RTL;
        }

        //generate default slug if not exists
        if(!$this->slug)
        {
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $this->name)));
            $this->slug = $slug;
        }
    }

    /**
     * Before create the series
     */
    public function beforeValidationOnUpdate()
    {
        parent::beforeValidationOnUpdate();
        if($this->hasChanged('name') && !$this->hasChanged('slug'))
        {
            $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $this->name)));
            $this->slug = $slug;
        }
    }
}