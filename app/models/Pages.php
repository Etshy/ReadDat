<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 18/11/2017
 * Time: 21:32
 */

namespace App\Models;


class Pages extends BaseModel
{

    /**
     * @var int
     */
    protected $chapterId;

    /**
     * @var int
     */
    protected $position;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var string
     */
    protected $mime;

    /**
     * @var string
     */
    protected $localPath;

    /**
     * @var string
     */
    protected $urlPath;

    /**
     * @return string
     */
    public function getLocalPath(): string
    {
        return $this->localPath;
    }

    /**
     * @param string $localPath
     */
    public function setLocalPath(string $localPath)
    {
        $this->localPath = $localPath;
    }

    /**
     * @return string
     */
    public function getUrlPath(): string
    {
        return $this->urlPath;
    }

    /**
     * @param string $urlPath
     */
    public function setUrlPath(string $urlPath)
    {
        $this->urlPath = $urlPath;
    }

    /**
     * @return int
     */
    public function getChapterId(): int
    {
        return $this->chapterId;
    }

    /**
     * @param int $chapterId
     */
    public function setChapterId(int $chapterId)
    {
        $this->chapterId = $chapterId;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getMime(): string
    {
        return $this->mime;
    }

    /**
     * @param string $mime
     */
    public function setMime(string $mime)
    {
        $this->mime = $mime;
    }

    public function initialize()
    {
        //1 series has multiple chapter so chapter belongs to series
        $this->belongsTo(
            'chapterId',
            Chapter::class,
            'id',
            [
            'alias' => 'chapter',
            'reusable' => true
        ]);
    }
}