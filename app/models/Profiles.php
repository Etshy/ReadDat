<?php
namespace App\Models;

use App\Models\Interfaces\ModelSoftDeletableInterface;
use App\Models\Traits\ModelSoftDeletableTrait;
use Phalcon\Mvc\Model;

/**
 * Vokuro\Models\Profiles
 * All the profile levels in the application. Used in conjenction with ACL lists
 */
class Profiles extends BaseModel implements ModelSoftDeletableInterface
{
    use ModelSoftDeletableTrait;

    /**
     * Default roles - not deletable
     * AND DON'T DELETE THEM MANUALLY!
     */
    const ADMIN = 1;
    const USER = 2;

    /**
     * Name
     * @var string
     */
    protected $name;

    /**
     * Active
     * @var boolean
     */
    protected $active;

    /**
     * Deletable
     * @var boolean
     */
    protected $deletable;


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        return $this->deletable;
    }

    /**
     * @param bool $deletable
     */
    public function setDeletable($deletable)
    {
        $this->deletable = $deletable;
    }

    /**
     * Define relationships to Users and Permissions
     */
    public function initialize()
    {
        $this->hasMany('id', Users::class, 'profilesId', [
            'alias' => 'users',
            'foreignKey' => [
                'message' => 'Profile cannot be deleted because it\'s used on Users'
            ]
        ]);

        $this->hasMany('id', Permissions::class, 'profilesId', [
            'alias' => 'permissions'
        ]);
    }
}
