<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 22/10/2017
 * Time: 00:58
 */

namespace App\Models\Interfaces;

interface ModelSoftDeletableInterface
{
    public function isDeleted();

    public function setDeleted($deleted);
}