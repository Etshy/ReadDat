<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 14/10/2017
 * Time: 22:34
 */

namespace App\Models;


use App\Models\Interfaces\ModelSoftDeletableInterface;
use Phalcon\Mvc\Model;
use Phalcon\Annotations\Annotation;

class BaseModel extends Model
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return \DateTime::createFromFormat('Y-m-d H:i:s', $this->createdAt);
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return \DateTime::createFromFormat('Y-m-d H:i:s', $this->updatedAt);
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Before create the model assign a date
     */
    public function beforeValidationOnCreate()
    {
        $this->createdAt = (new \DateTime())->format(\DateTime::ISO8601);
    }

    /**
     * Before update the model assign a date
     */
    public function beforeValidationOnUpdate()
    {
        $this->updatedAt = (new \DateTime())->format(\DateTime::ISO8601);
    }

    public function initialize()
    {

    }
}