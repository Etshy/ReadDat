<?php
namespace App\Models;

use App\Models\Interfaces\ModelSoftDeletableInterface;
use App\Models\Traits\ModelSoftDeletableTrait;
use Phalcon\Mvc\Model;

/**
 * Permissions
 * Stores the permissions by profile
 */
class Permissions extends BaseModel
{

    /**
     *
     * @var integer
     */
    protected $profilesId;

    /**
     *
     * @var string
     */
    protected $resource;

    /**
     *
     * @var string
     */
    protected $action;

    /**
     * @return int
     */
    public function getProfilesId()
    {
        return $this->profilesId;
    }

    /**
     * @param int $profilesId
     */
    public function setProfilesId($profilesId)
    {
        $this->profilesId = $profilesId;
    }

    /**
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     */
    public function setResource($resource)
    {
        $this->resource = $resource;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    public function initialize()
    {
        $this->belongsTo('profilesId', __NAMESPACE__ . '\Profiles', 'id', [
            'alias' => 'profile'
        ]);
    }
}
