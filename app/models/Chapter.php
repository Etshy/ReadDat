<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 15/10/2017
 * Time: 00:33
 */

namespace App\Models;


use App\Models\Interfaces\ModelSoftDeletableInterface;
use App\Models\Traits\ModelSoftDeletableTrait;
use Phalcon\Mvc\Model\Relation;

class Chapter extends BaseModel implements ModelSoftDeletableInterface
{
    use ModelSoftDeletableTrait;
    /**
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $seriesId;

    /**
     * @var int
     */
    protected $number;

    /**
     * @var int
     */
    protected $subNumber;

    /**
     * @var boolean
     */
    protected $visibility;

    /**
     * @var string
     */
    protected $language;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSeriesId()
    {
        return $this->seriesId;
    }

    /**
     * @param int $seriesId
     */
    public function setSeriesId($seriesId)
    {
        $this->seriesId = $seriesId;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getSubNumber()
    {
        return $this->subNumber;
    }

    /**
     * @param int $subNumber
     */
    public function setSubNumber($subNumber)
    {
        $this->subNumber = $subNumber;
    }

    /**
     * @return bool
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * @param bool $visibility
     */
    public function setVisibility(bool $visibility = false)
    {
        $this->visibility = $visibility;
    }

    public function initialize()
    {
        //1 series has multiple chapter so chapter belongs to series
        $this->belongsTo('seriesId', __NAMESPACE__ . '\Series', 'id', [
            'alias' => 'series',
            "foreignKey" => [
                "message" => _('')
            ]
        ]);

        $this->hasMany(
            'id',
            Pages::class,
            'chapterId',
            [
                'alias' => 'pages',
            ]
        );

        $this->hasManyToMany(
            'id',
            ChapterTeams::class,
            'chapterId',
            'teamsId',
            Teams::class,
            'id',
            [
                'alias' => 'Teams'
            ]
        );
    }

}