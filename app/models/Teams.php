<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 18/11/2017
 * Time: 22:50
 */

namespace App\Models;


use App\Models\Traits\ModelSoftDeletableTrait;

class Teams extends BaseModel
{

    use ModelSoftDeletableTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $website;

    /**
     * @var string
     */
    protected $irc;

    /**
     * @var string
     */
    protected $discord;

    /**
     * @var string
     */
    protected $twitter;

    /**
     * @var string
     */
    protected $facebook;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getWebsite(): string
    {
        return $this->website;
    }

    /**
     * @param string $website
     */
    public function setWebsite(string $website)
    {
        $this->website = $website;
    }

    /**
     * @return string
     */
    public function getIrc(): string
    {
        return $this->irc;
    }

    /**
     * @param string $irc
     */
    public function setIrc(string $irc)
    {
        $this->irc = $irc;
    }

    /**
     * @return string
     */
    public function getDiscord(): string
    {
        return $this->discord;
    }

    /**
     * @param string $discord
     */
    public function setDiscord(string $discord)
    {
        $this->discord = $discord;
    }

    /**
     * @return string
     */
    public function getTwitter(): string
    {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     */
    public function setTwitter(string $twitter)
    {
        $this->twitter = $twitter;
    }

    /**
     * @return string
     */
    public function getFacebook(): string
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     */
    public function setFacebook(string $facebook)
    {
        $this->facebook = $facebook;
    }

    public function initialize()
    {
        $this->hasManyToMany(
            'id',
            Teams::class,
            'teamsId',
            'chapterId',
            ChapterTeams::class,
            'id',
            [
                'alias' => 'chapter'
            ]
        );
    }

    /**
     * Before create the model
     */
    public function beforeValidationOnCreate()
    {
        parent::beforeValidationOnCreate();
    }

    /**
     * Before create the model
     */
    public function beforeValidationOnUpdate()
    {
        parent::beforeValidationOnUpdate();
    }
}