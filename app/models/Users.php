<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 14/10/2017
 * Time: 21:55
 */

namespace App\Models;


use App\Models\Interfaces\ModelSoftDeletableInterface;
use App\Models\Traits\ModelSoftDeletableTrait;

class Users extends BaseModel implements ModelSoftDeletableInterface
{
    use ModelSoftDeletableTrait;

    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $email;
    /**
     * @var string
     */
    protected $password;
    /**
     * @var string
     */
    protected $firstname;
    /**
     * @var string
     */
    protected $lastname;
    /**
     *
     * @var string
     */
    protected $mustChangePassword;

    /**
     *
     * @var string
     */
    protected $profilesId;

    /**
     *
     * @var string
     */
    protected $banned;

    /**
     *
     * @var string
     */
    protected $suspended;

    /**
     *
     * @var string
     */
    protected $active;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Users
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Users
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return Users
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return Users
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
        return $this;
    }

    /**
     * @return string
     */
    public function getMustChangePassword()
    {
        return $this->mustChangePassword;
    }

    /**
     * @param string $mustChangePassword
     * @return Users
     */
    public function setMustChangePassword($mustChangePassword)
    {
        $this->mustChangePassword = $mustChangePassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfilesId()
    {
        return $this->profilesId;
    }

    /**
     * @param string $profilesId
     * @return Users
     */
    public function setProfilesId($profilesId)
    {
        $this->profilesId = $profilesId;
        return $this;
    }

    /**
     * @return string
     */
    public function getBanned()
    {
        return $this->banned;
    }

    /**
     * @param string $banned
     * @return Users
     */
    public function setBanned($banned)
    {
        $this->banned = $banned;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuspended()
    {
        return $this->suspended;
    }

    /**
     * @param string $suspended
     * @return Users
     */
    public function setSuspended($suspended)
    {
        $this->suspended = $suspended;
        return $this;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param string $active
     * @return Users
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * Before create the user assign a password
     */
    public function beforeValidationOnCreate()
    {

        if (empty($this->password)) {
            // Generate a plain temporary password
            $tempPassword = preg_replace('/[^a-zA-Z0-9]/', '', base64_encode(openssl_random_pseudo_bytes(12)));
            // The user must change its password in first login
            $this->mustChangePassword = 1;
            // Use this password as default
            $this->password = $this->getDI()
                ->get('security')
                ->hash($tempPassword);
        } else {
            // The user must not change its password in first login
            $this->mustChangePassword = 0;
            $this->password = $this->getDI()->get('security')->hash($this->password);
        }

        // The account must be confirmed via e-mail
        // Only require this if emails are turned on in the config, otherwise account is automatically active
        if ($this->getDI()->get('config')->useMail) {
            $this->active = false;
        } else {
            $this->active = true;
        }
        // The account is not suspended by default
        $this->suspended = false;
        // The account is not banned by default
        $this->banned = false;
        if(empty($this->profilesId))
        {
            $this->profilesId = Profiles::USER;
        }
    }

    /**
     * Send a confirmation e-mail to the user if the account is not active
     */
    public function afterSave()
    {
       $this->getDI()->get('UserService')->sendConfirmationEmail($this);
    }

    public function initialize()
    {
        parent::initialize();
        $this->belongsTo('profilesId', Profiles::class, 'id', [
            'alias' => 'profile',
            'reusable' => true
        ]);

        $this->hasMany('id', SuccessLogins::class, 'usersId', [
            'alias' => 'successLogins',
            'foreignKey' => [
                'message' => 'User cannot be deleted because he/she has activity in the system'
            ]
        ]);

        $this->hasMany('id', PasswordChanges::class, 'usersId', [
            'alias' => 'passwordChanges',
            'foreignKey' => [
                'message' => 'User cannot be deleted because he/she has activity in the system'
            ]
        ]);

        $this->hasMany('id', ResetPasswords::class, 'usersId', [
            'alias' => 'resetPasswords',
            'foreignKey' => [
                'message' => 'User cannot be deleted because he/she has activity in the system'
            ]
        ]);
    }
}