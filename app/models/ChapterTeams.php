<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 15/10/2017
 * Time: 00:57
 */

namespace App\Models;


class ChapterTeams extends BaseModel
{

    protected $id;

    protected $chapterId;

    protected $teamsId;

    public function initialize()
    {

        $this->setSource('chapter_teams');

        $this->belongsTo(
            'chapterId',
            Chapter::class,
            'id'
        );
        $this->belongsTo(
            'teamsId',
            Teams::class,
            'id'
        );

    }
}