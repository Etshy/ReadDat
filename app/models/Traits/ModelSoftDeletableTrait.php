<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 22/10/2017
 * Time: 00:52
 */

namespace App\Models\Traits;


trait ModelSoftDeletableTrait
{

    /**
     * @var bool
     */
    protected $deleted;

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    public function beforeDelete()
    {
        //We update the model
        $this->setDeleted(true);
        $this->save();
        //TODO find a way to retrieve relation hasMany and launch a delete for the related records

        //return false to stop the deletion
        return false;
    }
}