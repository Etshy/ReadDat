<?php

namespace App\Controllers;

use App\Controllers\ControllerBase;
use App\Forms\AddChapterForm;

class PublicController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();

        //TODO save the text as default footer text in DB appsettings.
        $this->view->setVar('footerText', 'If you liked any of the manga you obtained here, consider buying the original versions, or the local translation, where available. Thanks for your support.');
        $this->view->setTemplateBefore('public');
    }
}