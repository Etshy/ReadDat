<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 02/12/2017
 * Time: 23:57
 */

namespace App\Controllers\Admin;


use App\Forms\TeamsForm;
use App\Models\Teams;
use App\Services\TeamsService;

class TeamsController extends LoggedInController
{

    public function indexAction()
    {
        $teamsService = $this->di->get('TeamsService');
        /**
         * @var $teamsService TeamsService
         */
        try{
            $teams = $teamsService->getTeamsByParam();
        } catch(\Exception $e){
            var_dump($e->getMessage());exit;
        } catch(\Error $e){
            var_dump($e->getMessage());exit;
        }

        $this->view->teams = $teams;
    }

    public function addAction(int $teamId = null)
    {
        $form = new TeamsForm();
        $formClass = '';
        $errorField = [];
        try {
            $teamsService = $this->di->get('TeamsService');
            /**
             * @var $teamsService TeamsService
             */

            if($teamId) {
                $team = $teamsService->getTeamsById($teamId);
                $form->setEntity($team);
            } else {
                $team = new Teams();
            }

            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {

                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    $errorField = $form->getErrorFiedsArray();
                } else {
                    //Valid form, we create the series
                    $form->bind($this->request->getPost(), $team);
                    $teamsService->saveTeams($team);
                    $this->response->redirect('admin/teams/');
                }
            }
        } catch (\Exception $e) {
            $this->flashSession->error($e->getMessage());
        } catch (\Error $e) {
            $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
        }
        $this->view->errorField = $errorField;
        $this->view->formClass = $formClass;
        $this->view->form = $form;
        $this->view->pick('teams/add');
    }

    public function editAction($teamId)
    {
        return $this->addAction($teamId);
    }
}