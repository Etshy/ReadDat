<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 14/10/2017
 * Time: 21:01
 */

namespace App\Controllers\Admin;


use App\Controllers\ControllerBase;
use Phalcon\Mvc\Dispatcher;

class LoggedInController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();

        $this->view->setTemplateBefore('private');
    }

    /**
     * loggedin index page
     */
    public function indexAction()
    {
        echo "logged in index";
    }

    public function rebuildAclAction()
    {
        $this->acl->rebuild();
        return $this->response->redirect($this->request->getHTTPReferer());
    }
}