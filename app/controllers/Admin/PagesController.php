<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 30/11/2017
 * Time: 23:42
 */

namespace App\Controllers\Admin;



use App\Services\PagesService;

class PagesController extends LoggedInController
{

    public function uploadPagesAction()
    {
        $this->view->disable();
        $response = new \Phalcon\Http\Response();
        //Order, chapterId, and seriesId is in $this->request->getPost()
        //File is in $this->request->getUploadedFiles() (array, so take the first item)
        $pagesService = $this->di->get('PagesService');
        /**
         * @var $pagesService PagesService
         */

        $chapterId = $this->request->getPost('chapterId');
        $seriesId = $this->request->getPost('seriesId');
        $order = $this->request->getPost('order');

        if($this->request->hasFiles() == true){
            try{
                $dataResponse = $pagesService->handleChapPageUpload($this->request->getUploadedFiles(), $seriesId, $chapterId, $order);
            } catch (ChapterPageException $e) {
                $response->setContent($e->getMessage());
                $response->setStatusCode(500);
                return $response;
            } catch(\Exception $e){
                $response->setContent($e->getMessage());
                $response->setStatusCode(500);
                return $response;
            } catch(\Error $e){
                $response->setContent($e->getMessage());
                $response->setStatusCode(500);
                return $response;
            }
        }

        //Set the content of the response
        $response->setContent(json_encode($dataResponse));
        //Return the response
        return $response;
    }

    public function deleteAction($pageId)
    {
        $this->view->disable();
        $response = new \Phalcon\Http\Response();

        try{
            $pagesService = $this->di->get('PagesService');
            /**
             * @var $pagesService PagesService
             */
            $return = $pagesService->deletePage($pageId);

            if (is_bool($return) && $return == true) {
                $response->setContent(json_encode([
                    'error' => $return
                ]));
            }

        } catch(\Exception $e){
            $response->setContent(json_encode([
                'error' => $e->getMessage()
            ]));
        } catch(\Error $e){
            $response->setContent(json_encode([
                'error' => $e->getMessage()
            ]));
        }
        return $response;
    }

    public function updatePagesOrderAction()
    {

        $this->view->disable();
        $response = new \Phalcon\Http\Response();



        $post = $this->request->getPut()['data'];
        $pagesService = $this->di->get('PagesService');
        /**
         * @var $pagesService PagesService
         */
        try{
            $pagesService->updatePagesOrder($post);
            $response->setJsonContent([
                'success' => true
            ]);
        } catch(\Exception $e){
            $response->setJsonContent([
                'error' => $e->getMessage()
            ]);
        } catch(\Error $e){
            $response->setJsonContent([
                'error' => $e->getMessage()
            ]);
        }

        return $response;
    }

}