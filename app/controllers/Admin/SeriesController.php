<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 24/10/2017
 * Time: 01:08
 */
namespace App\Controllers\Admin;

use App\Exceptions\ChapterPageException;
use App\Forms\AddChapterForm;
use App\Forms\SeriesForm;
use App\i18n\Locale;
use App\Models\Chapter;
use App\Models\Pages;
use App\Models\Series;
use App\Models\Teams;
use App\Services\ImageService;
use App\Services\PagesService;
use App\Services\SeriesService;
use App\Services\TeamsService;
use Phalcon\Exception;
use Phalcon\Mvc\Model\Resultset\Simple;

class SeriesController extends LoggedInController
{

    public function indexAction()
    {
        $seriesService = $this->di->get('SeriesService');
        if(!$seriesService instanceof SeriesService)
        {
            $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
        }
        $series = $seriesService->getSeriesByParam();

        $config = $this->di->get('config');
        $appConfig = $config->application;

        $this->view->appConfig = $appConfig;
        $this->view->series = $series;
    }

    public function addAction()
    {
        $form = new SeriesForm();
        $formClass = '';
        $errorField = [];
        try {
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {

                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    $errorField = $form->getErrorFiedsArray();
                } else {
                    //Valid form, we create the series
                    $serie = new Series();
                    $form->bind($this->request->getPost(), $serie);
                    $seriesService = $this->di->get('SeriesService');
                    /**
                     * @var SeriesService $seriesService
                     */
                    $seriesService->saveSeries($serie);

                    //redirect to addChapter page for this series.
                    $this->response->redirect('admin/series/add-chapter/'.$serie->getSlug());
                }
            }
        } catch (Exception $e) {
            $this->flashSession->error($e->getMessage());
        } catch (\Error $e) {
            $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
        }
        $this->view->errorField = $errorField;
        $this->view->formClass = $formClass;
        $this->view->form = $form;
        $this->view->pick('series/add');
    }

    public function editAction($slug)
    {
        $seriesService = $this->di->get('SeriesService');
        if(!$seriesService instanceof SeriesService)
        {
            $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
        }
        $imageService = $this->di->get('ImageService');
        if(!$imageService instanceof ImageService)
        {
            $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
        }

        $serie = $seriesService->getSeriesBySlug($slug);
        $form = new SeriesForm();
        $form->setEntity($serie);
        $formClass = '';
        $errorField = [];
        try {
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {
                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    $errorField = $form->getErrorFiedsArray();
                } else {
                    $oldSeriesImage = $serie->getImage();
                    //Valid form, we create the series
                    $form->bind($this->request->getPost(), $serie);
                    /**
                     * @var SeriesService $seriesService
                     */
                    $seriesService = $this->di->get('SeriesService');

                    $files = $this->request->getUploadedFiles();

                    if($this->request->hasFiles() == true && $files[0]->getSize() > 0){
                        $paths = $imageService->handleSeriesImage($files);
                        //Only one image expected, we always take the first
                        $serie->setImage($paths[0]);
                    } else {
                        $serie->setImage($oldSeriesImage);
                    }
                    $seriesService->saveSeries($serie);
                }
            }
        } catch (Exception $e) {
            $this->flashSession->error($e->getMessage());
        } catch (\Error $e) {
            var_dump($e->getMessage());exit;
            $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
        }

        $params = [
            'seriesId' => $serie->getId()
        ];
        $chapters = $seriesService->getChapterByParams($params);

        $this->view->chapters = $chapters;
        $this->view->series = $serie;
        $this->view->form = $form;
        $this->view->errorField = $errorField;
        $this->view->formClass = $formClass;
        $this->view->pick("series/add");
    }

    public function addChapterAction($seriesSlug)
    {
        $formClass = '';
        $errorField = [];
        try{
            $seriesService = $this->di->get('SeriesService');
            /**
             * @var $seriesService SeriesService
             */
            $teamsService = $this->di->get('TeamsService');
            /**
             * @var $teamsService TeamsService
             */

            $series = $seriesService->getSeriesBySlug($seriesSlug);

            $form = new AddChapterForm();

            if ($this->request->isPost()) {

                $post = $this->request->getPost();

                if ($form->isValid($post) == false) {

                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    $errorField = $form->getErrorFiedsArray();
                } else {

                    //Valid form, we create the series
                    $chapter = new Chapter();

                    $teamsArray = $post['teams'];
                    unset($post['teams']);

                    $form->bind($post, $chapter);
                    $chapter->setSeriesId($series->getId());

                    $teams = [];

                    foreach ($teamsArray as $teamId) {
                        $team = $teamsService->getTeamsById($teamId);
                        if($team instanceof Teams)
                        {
                            $teams[] = $team;
                        }
                    }

                    $chapter->teams = $teams;

                    //Save
                    $chapterResult = $seriesService->saveChapter($chapter);
                    if(!$chapterResult) {
                        $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
                    }
                    else {
                        $id = $chapter->getReadConnection()->lastInsertId();
                        //Redirect to editChapter to upload the pages
                        $this->response->redirect('admin/series/edit-chapter/'.$series->getSlug().'/'.$chapter->getId());
                    }
                }
            }

        }  catch (Exception $e) {
            var_dump($e->getMessage());echo '<br>';
            var_dump($e->getTraceAsString());exit;
            $this->flashSession->error($e->getMessage());
        } catch (\Error $e) {
            $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
        }

        $this->view->form = $form;
        $this->view->errorField = $errorField;
        $this->view->formClass = $formClass;
        $this->view->series = $series;
        $this->view->pick('series/add-chapter');
    }

    public function editChapterAction($seriesSlug, $chapterId)
    {
        $formClass = '';
        $errorField = [];


        try{

            $seriesService = $this->di->get('SeriesService');
            /**
             * @var $seriesService SeriesService
             */

            $series = $seriesService->getSeriesBySlug($seriesSlug);
            /**
             * @var $series Series
             */
            $chapter = $seriesService->getChapterById($chapterId);
            /**
             * @var $chapter Chapter
             */
            $teamsService = $this->di->get('TeamsService');
            /**
             * @var $teamsService TeamsService
             */

            $form = new AddChapterForm();
            $form->setEntity($chapter);

            $form->setMultiSelectDefaultValue(AddChapterForm::TEAMS, $chapter->getTeams());
            $form->populateCheckbox(AddChapterForm::VISIBILITY, $chapter->getVisibility());

            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost()) == false) {

                    foreach ($form->getMessages() as $message) {
                        $this->flashSession->error($message);
                    }
                    $errorField = $form->getErrorFiedsArray();
                } else {
                    //Valid form, we edit the chap
                    $post = $this->request->getPost();
                    $teamsArray = $post['teams'];
                    unset($post['teams']);

                    $chapter = new Chapter();

                    $form->bind($post, $chapter);
                    $chapter->setSeriesId($series->getId());

                    $teams = [];

                    foreach ($teamsArray as $teamId) {
                        $team = $teamsService->getTeamsById($teamId);

                        if($team instanceof Teams)
                        {
                            $teams[] = $team;
                        }
                    }

                    /**
                     * @var $chapter->teams Phalcon\Mvc\Model\Resultset\Simple
                     */

                    $seriesService->deleteChapterTeamsRelationshipFromChapter($chapter);

                    $chapter->teams = $teams;
                    //Save
                    $chapterResult = $seriesService->saveChapter($chapter);
                    if(!$chapterResult) {
                        //var_dump($chapter->getMessages());exit;
                        $this->flashSession->error('An unexpected error occurred.');
                    }
                    else {

                    }
                }
            }

            $pagesService = $this->di->get('PagesService');
            /**
             * @var $pagesService PagesService
             */
            $pages = $pagesService->getPagesByChapter($chapter->getId());

        }  catch (Exception $e) {
            $this->flashSession->error($e->getMessage());
        }
        /**
         * @var $chapter Simple
         */

        $this->view->form = $form;
        $this->view->errorField = $errorField;
        $this->view->formClass = $formClass;
        $this->view->chapter = $chapter;
        $this->view->series = $series;
        $this->view->config = $this->getDI()->getConfig();
        $this->view->pages = $pages;
        $this->view->pick('series/edit-chapter');

    }

    public function deleteSeriesAction($seriesSlug = null)
    {
        $seriesService = $this->di->get('SeriesService');
        if(!$seriesService instanceof SeriesService) {
            $this->flashSession->error('An unexpected error occurred. A mail was sent to administrator.');
            return $this->response->redirect($this->request->getHTTPReferer());
        }
        $series = $seriesService->getSeriesBySlug($seriesSlug);
        /**
         * @var $series Series
         */
        if(!$series instanceof Series) {
            $this->flashSession->error('Series not found');
            return $this->response->redirect($this->request->getHTTPReferer());
        }
        $series->delete();
        //FIXME hack to delete chapters..
        //TODO try to do something in ModelSoftDeletableTrait
        foreach ($series->getChapters() as $chapter) {
            $chapter->delete();
        }
        return $this->response->redirect($this->request->getHTTPReferer());
    }
}