<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 01/12/2017
 * Time: 23:45
 */

namespace App\Controllers\Admin;


use App\Services\UserService;

class UsersController extends LoggedInController
{

    public function indexAction()
    {
        $userService = $this->di->get('UserService');
        /**
         * @var $userService UserService
         */

        $users = $userService->getUsersByParam();

        $this->view->users = $users;
    }

    public function detailsAction($userId)
    {
        $userService = $this->di->get('UserService');
        /**
         * @var $userService UserService
         */
        $user = $userService->getUserById($userId);
        //TODO create UserDetailsForm

        $this->view->pick('users/details');
    }
    
}