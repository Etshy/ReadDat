<?php
namespace App\Controllers;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

class ControllerBase extends Controller
{
    public function initialize()
    {
        //Inject js and css in all pages/layouts from here.
        //If a layouts need specific js/css add an ACL condition.
        $headerCollection = $this->assets->collection('header');

        // Add some local CSS resources
        $headerCollection->addCss('semantic/dist/semantic.min.css');
        $headerCollection->addCss('css/main.css');

        $footerCollection = $this->assets->collection('footer');
        // And some JavaScript resources
        $headerCollection->addJs('https://code.jquery.com/jquery-3.1.1.min.js');
        $footerCollection->addJs('semantic/dist/semantic.min.js');
        $footerCollection->addJs('js/main.js');


        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
        $this->view->identity = $this->auth->getIdentity();
        $this->view->config = $this->getDI()->getConfig();
    }


    /**
     * Execute before the router so we can determine if this is a private controller, and must be authenticated, or a
     * public controller that is open to all.
     *
     * @param Dispatcher $dispatcher
     * @return boolean
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerClass();

        // Only check permissions on private controllers
        if ($this->acl->isPrivate($controllerName)) {

            // Get the current identity
            $identity = $this->auth->getIdentity();

            // If there is no identity available the user is redirected to index/index
            if (!is_array($identity)) {

                if($this->auth->hasRememberMe()) {
                    $this->auth->loginWithRememberMe();
                    $identity = $this->auth->getIdentity();
                } else {
                    $this->flashSession->error('You don\'t have access to this module: private');
                    $dispatcher->setNamespaceName('App\Controllers');
                    $dispatcher->forward([
                        'controller' => 'index',
                        'action' => 'index'
                    ]);
                    return false;
                }
            }

            // Check if the user have permission to the current option
            $actionName = $dispatcher->getActionName();

            //hack to have "real" camelCase, starting by lowercase char
            $actionName = lcfirst($actionName);


            if (!$this->acl->isAllowed($identity['profile'], $controllerName, $actionName)) {

                $this->flashSession->notice('You don\'t have access to this module: ' . $controllerName . ':' . $actionName);

                if ($this->acl->isAllowed($identity['profile'], $controllerName, 'index')) {

                    $dispatcher->setNamespaceName($dispatcher->getNamespaceName());
                    $dispatcher->forward([
                        'controller' => $dispatcher->getControllerName(),
                        'action' => 'index'
                    ]);
                } else {
                    $response = $this->response;

                    //force the namespace
                    $dispatcher->setNamespaceName('App\Controllers');
                    $dispatcher->forward([
                        'controller' => 'index',
                        'action' => 'index'
                    ]);
                }
                return false;
            }
        }
    }
}
