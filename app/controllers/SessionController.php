<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 14/10/2017
 * Time: 21:01
 */

namespace App\Controllers;


use App\Forms\LoginForm;
use App\Forms\RegisterForm;
use App\Models\Users;
use App\Security\Auth\Exception as AuthException;

class SessionController extends ControllerBase
{

    public function initialize()
    {
        parent::initialize();
        $this->view->setTemplateBefore('session');
    }

    public function loginAction()
    {
        $loginForm = new LoginForm();
        $formClass = '';
        $errorField = [];

        try {
            if (!$this->request->isPost()) {
                if ($this->auth->hasRememberMe()) {
                    return $this->auth->loginWithRememberMe(true);
                }
            } else {

                if ($loginForm->isValid($this->request->getPost()) == false) {
                    foreach ($loginForm->getMessages() as $message) {
                        $this->flash->error($message);
                    }
                } else {

                    $this->auth->check([
                        'name' => $this->request->getPost('name'),
                        'password' => $this->request->getPost('password'),
                        'remember' => $this->request->getPost('remember')
                    ]);
                    //TODO redirect to previousPage ?
                    return $this->response->redirect('admin/profile/');
                }
            }
        } catch (AuthException $e) {
            $this->flash->error($e->getMessage());
        }

        $this->view->errorField = $errorField;
        $this->view->formClass = $formClass;
        $this->view->loginForm = $loginForm;
        // Shows only the view related to the action
        $this->view->pick('session/login');
    }

    public function registerAction()
    {
        try {
            $registerForm = new RegisterForm();
            $formClass = '';
            $errorField = [];

            $userService = $this->di->get('UserService');

            if ($this->request->isPost()) {
                $post = $this->request->getPost();
                $user = new Users();

                if ($registerForm->isValid($post)) {

                    $registerForm->bind($_POST, $user);

                    $returnSave = $userService->saveUser($user);

                    if ($returnSave) {
                        return $this->response->redirect('register');
                    }

                    $this->flash->error($user->getMessages());
                } else {
                    $formClass = ' error ';
                    $errorField = $registerForm->getErrorFiedsArray();
                }
            }
        } catch (\Exception $e) {
            var_dump($e);exit;
            $this->flash->error($e->getMessage());
        } catch (\Error $e) {
            var_dump($e->getMessage());exit;
        }

        $this->view->errorField = $errorField;
        $this->view->formClass = $formClass;
        $this->view->registerForm = $registerForm;
        // Shows only the view related to the action

        $this->view->pick('session/register');
    }

    /**
     * Closes the session
     */
    public function logoutAction()
    {
        $this->auth->remove();
        return $this->response->redirect('');
    }
}