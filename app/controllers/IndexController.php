<?php

namespace App\Controllers;

use App\Services\SeriesService;
use Phalcon\Http\Request\File;
use Phalcon\Paginator\Adapter\QueryBuilder  as PaginatorModel;

class IndexController extends PublicController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {
        $seriesService = $this->di->get('SeriesService');
        /**
         * @var $seriesService SeriesService
         */
        $chaptersQB = $seriesService->getLastestChaptersForHomepage();
        //homepage so we display the 1st page
        $currentPage = 1;
        $paginator = new PaginatorModel(
            [
                'builder'  => $chaptersQB,
                'limit' => 30,
                'page'  => $currentPage,
            ]
        );

        $this->view->page = $paginator->getPaginate();
    }
}

