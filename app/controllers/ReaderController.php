<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 06/12/2017
 * Time: 00:33
 */

namespace App\Controllers;


use App\Models\Chapter;
use App\Models\Series;
use App\Services\PagesService;
use App\Services\SeriesService;

class ReaderController extends PublicController
{

    public function initialize()
    {
        parent::initialize();
    }

    public function indexAction()
    {
        $seriesSlug = $this->dispatcher->getParam('seriesSlug');
        $chapterNumber = (int)$this->dispatcher->getParam('chapterNumber');
        $chapterSubNumber = (int)$this->dispatcher->getParam('chapterSubNumber');
        $pageNumber = (int)$this->dispatcher->getParam('pageNumber');
        if(!$pageNumber) {
            $pageNumber = 1;
        }


        $footerCollection = $this->assets->collection('footer');
        $headerCollection = $this->assets->collection('header');
        $footerCollection->addJs('https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js');




        //dev
        /*
        $footerCollection->addJs('js/developer_version/jquery.browser.js');
        $footerCollection->addJs('js/developer_version/jquery.ba-hashchange.js');
        $footerCollection->addJs('js/developer_version/jquery.easing.custom.1.3.js');
        $footerCollection->addJs('js/developer_version/modernizr.js');
        $footerCollection->addJs('js/developer_version/hammer.js');
        $footerCollection->addJs('js/developer_version/wow_book.js');
        */

        $headerCollection->addCss('js/wow_book/wow_book.css');

        $seriesService = $this->di->get('SeriesService');
        /**
         * @var $seriesService SeriesService
         */

        $series = $seriesService->getSeriesBySlug($seriesSlug);
        /**
         * @var $serie Series
         */

        $pagesService = $this->di->get('PagesService');
        /**
         * @var $pagesService PagesService
         */

        $params=[
            "chapterNumber" => $chapterNumber,
            "seriesId" => $series->getId()
        ];
        if(!is_null($chapterSubNumber)) {
            $params["chapterSubNumber"] = $chapterSubNumber;
        }
        $resultSet = $seriesService->getChapterByParams($params);
        $chapter = $resultSet->getFirst();

        if($chapter === false) {
            //TODO return 404
        }

        $paramsList = [
            "seriesId" => $series->getId()
        ];
        $chapterList = $seriesService->getChapterByParams($paramsList);

        //TODO load cookies to check browser reader preferences
        $forceReaderMode = false;
        $forceReaderDirection = false;



        if($this->cookies->has('READER_MODE') && !empty($this->cookies->get('READER_MODE')->getValue()))
        {
            $forceReaderMode = $this->cookies->get('READER_MODE')->getValue();

            if($forceReaderMode == Series::READER_MODE_FLIPBOOK) {
                if($this->cookies->has('READER_DIRECTION') && !empty($this->cookies->get('READER_DIRECTION')->getValue())) {
                    $forceReaderDirection = $this->cookies->get('READER_DIRECTION')->getValue();
                } else {
                    //reset - can't have flipbook without direction
                    $forceReaderMode = false;
                    $forceReaderDirection = false;
                }
            }
        }
        if($forceReaderDirection !== false) {
            //from user's pref or cookies
            $readerDirection = $forceReaderDirection;
        }elseif($series->getReadDirection()){
            $readerDirection = $series->getReadDirection();
        } else {
            //default
            $readerDirection = \App\Models\Series::READ_DIRECTION_LTR;
        }



        if($forceReaderMode !== false){
            //from user's pref or cookies
            $readerMode = $forceReaderMode;
        }elseif($series->getReaderMode()) {
            $readerMode = $series->getReaderMode();
        } else {
            //default
            $readerMode = \App\Models\Series::READER_MODE_CLASSIC;
        }

        if($readerMode == Series::READER_MODE_CLASSIC){
            //Add this only if we use classic view. It cause problem with the flipbook plugin
            $footerCollection->addJs('js/jquery.history.js');
        } elseif ($readerMode == Series::READER_MODE_FLIPBOOK) {
            $footerCollection->addJs('js/wow_book/wow_book.min.js');
        }



        $this->view->pick('reader/index');
        $this->view->chapter = $chapter;
        $this->view->chapterList = $chapterList;
        $this->view->series = $series;
        $this->view->pages =  $pagesService->getPagesByChapter($chapter->getId());
        $this->view->currentPage = $pageNumber;

        $this->view->readerMode = $readerMode;
        $this->view->readerDirection = $readerDirection;

    }

    public function cookiesAction()
    {
        $this->view->disable();

        $post = $this->request->getPost();
        if(array_key_exists('deleteCookies', $post) && $post['deleteCookies'] == true) {
            if($this->cookies->has('READER_MODE')){
                $this->cookies->get('READER_MODE')->delete();
            }
            if($this->cookies->has('READER_DIRECTION')){
                $this->cookies->get('READER_DIRECTION')->delete();
            }
        } else {
            if(array_key_exists('READER_MODE', $post)){
                switch ($post['READER_MODE']){
                    case Series::READER_MODE_FLIPBOOK:
                        if(!array_key_exists('READER_DIRECTION', $post)){
                            $this->response->setJsonContent([
                                'error' => "error : wrong data sent - can't choose flipbook without specify direction"
                            ]);
                            $this->response->setStatusCode(400);
                            return $this->response;
                        }
                        $this->cookies->set('READER_DIRECTION', $post['READER_DIRECTION']);

                    case Series::READER_MODE_CLASSIC:
                    case Series::READER_MODE_WEBTOON:
                        $this->cookies->set('READER_MODE', $post['READER_MODE']);
                        break;

                    default:
                        $this->response->setJsonContent([
                            'error' => "error : wrong data sent - unknown options"
                        ]);
                        $this->response->setStatusCode(400);
                        return $this->response;
                        break;
                }
            }

        }

        $this->response->setJsonContent([
            'success' => true
        ]);

        return $this->response;

    }
}