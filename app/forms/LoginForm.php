<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 14/10/2017
 * Time: 21:50
 */
namespace App\Forms;

use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;

class LoginForm extends BaseForm
{
    const USERNAME = 'name';
    const PASSWORD = 'password';

    public function initialize()
    {
        $name = new Text(self::USERNAME);
        $name->setLabel('Username');
        $name->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $name->addValidators([
            new PresenceOf([
                'message' => 'The username is required',
            ])
        ]);
        $this->add($name);

        $password = new Password(self::PASSWORD);
        $password->setLabel('Password');
        $password->addValidators([
            new PresenceOf([
                'message' => 'The password is required'
            ])
        ]);
        $this->add($password);


        // Remember
        $remember = new Check('remember', [
            'value' => 'yes'
        ]);
        $remember->setAttribute('class', 'hide');
        $remember->setLabel('Remember me');
        $this->add($remember);

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);


        // Login btn
        $submit = new Submit('Log In', [
            'class' => 'ui button green fluid'
        ]);
        $submit->setLabel('Log In');
        $this->add($submit);
    }
}