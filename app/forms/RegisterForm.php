<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 21/10/2017
 * Time: 16:32
 */

namespace App\Forms;


use App\Models\Users;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\Confirmation;
use Phalcon\Validation\Validator\Identical;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Uniqueness;

class RegisterForm extends BaseForm
{
    const USERNAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const CONFIRM_PASSWORD = 'confirm_password';
    const FIRST_NAME = 'firstname';
    const LAST_NAME = 'lastname';

    public function initialize()
    {
        $name = new Text(self::USERNAME);
        $name->setLabel('Username');
        $name->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $name->addValidators([
            new PresenceOf([
                'message' => 'The username is required',
            ]),
            new Uniqueness([
                "model" => new Users(),
                "message" => "The username is already registered"
            ])
        ]);
        $this->add($name);

        $email = new Email(self::EMAIL);
        $email->setLabel('Email Address');
        $email->setFilters([
            'string',
            'trim',
            'email'
        ]);
        $email->addValidators([
            new PresenceOf([
                'message' => 'The email is required'
            ]),
            new \Phalcon\Validation\Validator\Email([
                'message' => 'The email is not valid'
            ]),
            new Uniqueness([
                "model" => new Users(),
                "message" => "The email is already registered"
            ])
        ]);
        $this->add($email);


        $password = new Password(self::PASSWORD);
        $password->setLabel('Password');
        $password->addValidators([
            new PresenceOf([
                'message' => 'The password is required'
            ]),
            new StringLength([
                'min' => 8,
                'messageMinimum' => 'Password is too short. Minimum 8 characters'
            ]),
            new Confirmation([
                'message' => 'Password doesn\'t match confirmation',
                'with' => self::CONFIRM_PASSWORD
            ])
        ]);
        $this->add($password);

        // Confirm Password
        $confirmPassword = new Password(self::CONFIRM_PASSWORD);
        $confirmPassword->setLabel('Confirm Password');
        $confirmPassword->addValidators([
            new PresenceOf([
                'message' => 'The confirmation password is required'
            ])
        ]);
        $this->add($confirmPassword);

        //Additionnal Information
        $firstName = new Text(self::FIRST_NAME);
        $firstName->setLabel('First Name');
        $firstName->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($firstName);

        $lastName = new Text(self::LAST_NAME);
        $lastName->setLabel('Last Name');
        $lastName->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($lastName);

        // CSRF
        $csrf = new Hidden('csrf');
        $csrf->addValidator(new Identical([
            'value' => $this->security->getSessionToken(),
            'message' => 'CSRF validation failed'
        ]));
        $csrf->clear();
        $this->add($csrf);

        // Sign Up
        $submit = new Submit('Register', [
            'class' => 'ui button green fluid'
        ]);
        $submit->setLabel('Register');
        $this->add($submit);
    }

}