<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 03/12/2017
 * Time: 00:15
 */

namespace App\Forms;


use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;

class TeamsForm extends BaseForm
{

    const NAME = 'name';
    const WEBSITE = 'website';
    const IRC = 'irc';
    const DISCORD = 'discord';
    const TWITTER = 'twitter';
    const FACEBOOK = 'facebook';

    public function initialize()
    {
        $id = new Hidden('id');
        $this->add($id);

        // name
        $name = new Text(self::NAME);
        $name->setLabel('Name');
        $name->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $name->addValidators([
            new PresenceOf([
                'message' => 'The team\'s name is required',
            ])
        ]);
        $this->add($name);

        //website url
        $website = new Text(self::WEBSITE);
        $website->setLabel('Website');
        $website->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($website);

        //irc
        $irc = new Text(self::IRC);
        $irc->setLabel('IRC');
        $irc->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($irc);

        //discord
        $discord = new Text(self::DISCORD);
        $discord->setLabel('Discord');
        $discord->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($discord);

        //twitter
        $twitter = new Text(self::TWITTER);
        $twitter->setLabel('Twitter');
        $twitter->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($twitter);

        //facebook
        $facebook = new Text(self::FACEBOOK);
        $facebook->setLabel('Facebook');
        $facebook->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($facebook);

        // Submit btn
        $submit = new Submit('Save', [
            'class' => 'ui button green fluid'
        ]);
        $submit->setLabel('Save');
        $this->add($submit);
    }

}