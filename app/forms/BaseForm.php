<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 21/10/2017
 * Time: 18:53
 */

namespace App\Forms;


use App\Models\BaseModel;
use Phalcon\Forms\Element;
use Phalcon\Forms\Exception;
use Phalcon\Forms\Form;
use Phalcon\Mvc\Model\Resultset;
use Phalcon\Validation\Validator\PresenceOf;

class BaseForm extends Form
{
    /**
     * Get error fields
     * @return array
     */
    public function getErrorFiedsArray()
    {
        $errorField = [];
        if ($this->getMessages())
        {
            $messages = $this->getMessages();
            foreach ($messages as $field => $message)
            {
                $errorField[$message->getField()] = ' error ';
            }
        }
        return $errorField;
    }

    public function isFieldRequired($field)
    {
        if ($this->has($field)) {
            $element = $this->get($field);
            if ($element->getValidators()){
                foreach ($element->getValidators() as $validator){
                    if ($validator instanceof PresenceOf){
                        //We have a PresenceOf validator, so the input is mandatory
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Prints messages for a specific element
     */
    public function messages($name)
    {
        if ($this->hasMessagesFor($name)) {
            foreach ($this->getMessagesFor($name) as $message) {
                $this->flash->error($message);
            }
        }
    }

    /**
     * Set default values for a multiple select from array of Model
     *
     * @param string $fieldName
     * @param Resultset $values
     * @throws Exception
     */
    public function setMultiSelectDefaultValue(string $fieldName, Resultset $values)
    {
        if(!$this->has($fieldName)) {
            throw new Exception('Field not found in the Form');
        }
        $element = $this->get($fieldName);
        if(! $element instanceof Element\Select) {
            throw new Exception('Element is not a Select');
        }
        if( ! array_key_exists('multiple', $element->getAttributes()) || $element->getAttributes()['multiple'] == false ) {
            throw new Exception('Element is not a multiple Select');
        }
        $defaultValues = [];
        foreach ($values as $model){
            if($model instanceof BaseModel) {
                $defaultValues[] = $model->getId();
            }
        }
        $element->setDefault($defaultValues);
    }

    public function populateCheckbox(string $fieldName, bool $value)
    {
        if(!$this->has($fieldName)) {
            throw new Exception('Field not found in the Form');
        }
        $element = $this->get($fieldName);
        if(! $element instanceof Element\Check) {
            throw new Exception('Element is not a Checkbox');
        }
        if($value == $element->getDefault()) {
            $element->setDefault($value);
        } else {
            $element->setDefault(0);
        }



    }

}