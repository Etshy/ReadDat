<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 14/11/2017
 * Time: 01:13
 */

namespace App\Forms;
use App\i18n\Locale;
use App\Models\BaseModel;
use App\Models\Teams;
use App\Services\TeamsService;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;


/**
 * Class AddChapterForm
 * @package App\Forms
 */
class AddChapterForm extends BaseForm
{

    const NAME = 'name';
    const NUMBER = 'number';
    const SUBNUMBER = 'subNumber';
    const LANGUAGE = 'language';
    const VISIBILITY = 'visibility';
    const TEAMS = 'teams';

    public function initialize()
    {

        $id = new Hidden('id');
        $this->add($id);

        $createdAt = new Hidden('createdAt');
        $this->add($createdAt);

        $deleted = new Hidden('deleted');
        $this->add($deleted);

        //name
        $name = new Text(self::NAME);
        $name->setLabel('Name');
        $name->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $name->addValidators([
        ]);
        $this->add($name);

        $number = new Numeric(self::NUMBER);
        $number->setLabel('Number');
        $number->setFilters([
            'int'
        ]);
        $number->addValidators([
            new PresenceOf([
                'message' =>  'The number is required'
            ]),
        ]);
        $this->add($number);

        $subnumber = new Numeric(self::SUBNUMBER);
        $subnumber->setLabel('Sub number');
        $subnumber->setFilters([
            'int'
        ]);
        $this->add($subnumber);

        $language = new Select(self::LANGUAGE);
        $language->setLabel('Language');
        $locale = $this->di->get('Locale');
        /**
         * @var $locale \Locale
         */
        $languages = $locale->getLanguageList();
        $language->setOptions($languages);
        $language->setDefault($locale->getLanguage());
        $language->setAttribute('class', 'ui search dropdown');
        $this->add($language);

        $visibility = new Check(self::VISIBILITY);
        $visibility->setLabel('Visible');
        $visibility->setDefault(true);
        $visibility->setAttribute('value', true);
        $this->add($visibility);

        $teams = new Select(self::TEAMS);
        $teams->setLabel('Teams');
        $teamsService = $this->di->get('TeamsService');
        /**
         * @var $teamsService TeamsService
         */
        $teamsList = $teamsService->getTeamsByParam();
        $teamsListOptions = [];
        foreach ($teamsList as $team) {
            /**
             * @var $team Teams
             */
            $teamsListOptions[$team->getId()] = $team->getName();
        }
        asort($teamsListOptions);
        $teams->setOptions($teamsListOptions);
        $teams->setAttribute('class', 'ui search dropdown');
        $teams->setAttribute('multiple', true);
        $teams->addValidators([
            new PresenceOf([
                'message' =>  'team is required'
            ]),
        ]);
        $this->add($teams);

        // Submit btn
        $submit = new Submit('Save', [
            'class' => 'ui button green fluid'
        ]);
        $submit->setLabel('Save');
        $this->add($submit);
    }
}