<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 26/10/2017
 * Time: 00:43
 */

namespace App\Forms;


use App\Models\Series;
use App\Models\Users;
use Phalcon\Forms\Element\Check;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness;

class SeriesForm extends BaseForm
{

    const NAME = 'name';
    const ADDITIONAL_NAMES = 'additional_names';
    const SLUG = 'slug';
    const IMAGE = 'image';
    const AUTHOR = 'author';
    const ARTIST = 'artist';
    const DESCRIPTION = 'description';
    const ADULT_WARNING = 'adult_warning';
    const READER_MODE = 'reader_mode';
    const READ_DIRECTION = 'read_direction';
    const VISIBILITY = 'visibility';
    const CUSTOM_TITLE = 'custom_title';

    public function initialize()
    {
        $id = new Hidden('id');
        $this->add($id);

        //Main name
        $name = new Text(self::NAME);
        $name->setLabel('Series Name');
        $name->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $name->addValidators([
            new PresenceOf([
                'message' => 'The series name is required',
            ])
        ]);
        $this->add($name);

        //Additional names
        $addName = new Text(self::ADDITIONAL_NAMES);
        $addName->setLabel('Additional Names');
        $addName->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($addName);

        //slug
        $slug = new Text(self::SLUG);
        $slug->setLabel('Slug (custom url for the series) (not yet implemented)');
        $slug->setFilters([
            'string',
            'trim',
            'url',
        ]
        );
        $slug->setAttribute('disabled', true);
        $slug->addValidators([
            new Uniqueness([
                "model" => new Series(),
                "message" => "The slug is already registered"
            ])
        ]);
        $this->add($slug);

        //Image file
        $image = new File(self::IMAGE);
        $image->setLabel('Image');
        $image->addValidator(new \Phalcon\Validation\Validator\File([
            'allowEmpty' => true,
            'allowedTypes' => [
                'image/jpeg',
                'image/png'
            ],
            "messageType" => "Allowed file types are :types",
        ]));
        $this->add($image);

        //Author
        $author = new Text(self::AUTHOR);
        $author->setLabel('Authors');
        $author->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $author->addValidators([
            new PresenceOf([
                'message' => 'The author is required',
            ])
        ]);
        $this->add($author);

        //Artists
        $artists = new Text(self::ARTIST);
        $artists->setLabel('Artist');
        $artists->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $artists->addValidators([
            new PresenceOf([
                'message' => 'The artist is required',
            ])
        ]);
        $this->add($artists);

        //Description
        $description = new TextArea(self::DESCRIPTION);
        $description->setLabel('Description');
        $description->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $description->setAttribute('rows', 4);
        $this->add($description);

        //AdultWarning
        $adultWarning = new Check(self::ADULT_WARNING);
        $adultWarning->setLabel('Adult warning ?');
        $this->add($adultWarning);

        //Reader mode
        $readerMode = new Select(self::READER_MODE);
        $readerMode->setLabel('Reader mode');
        $readerMode->setOptions([
            Series::READER_MODE_CLASSIC => 'Classic',
            Series::READER_MODE_FLIPBOOK => 'Book-like',
            Series::READER_MODE_WEBTOON => 'Webtoon',
        ]);
        $this->add($readerMode);

        //read direction
        $readDirection = new Select(self::READ_DIRECTION);
        $readDirection->setLabel('Read direction');
        $readDirection->setOptions([
            Series::READ_DIRECTION_RTL => 'Right to Left',
            Series::READ_DIRECTION_LTR => 'Left to Right',
        ]);
        $this->add($readDirection);

        //Visibility
        $visibility = new Check(self::VISIBILITY);
        $visibility->setLabel('Visible ?');
        $this->add($visibility);

        //Description
        $customTitle = new Text(self::CUSTOM_TITLE);
        $customTitle->setLabel('Custom title');
        $customTitle->setFilters(
            [
                'string',
                'trim',
            ]
        );
        $this->add($customTitle);

        // Submit btn
        $submit = new Submit('Save', [
            'class' => 'ui button green fluid'
        ]);
        $submit->setLabel('Save');
        $this->add($submit);

    }
}