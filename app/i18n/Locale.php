<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 16/11/2017
 * Time: 00:42
 */

namespace App\i18n;


use Phalcon\Di\FactoryDefault;
use Phalcon\Exception;
use Phalcon\Http\Request;

class Locale extends \Locale
{
    /**
     * @var string
     */
    protected $languageCode;

    /**
     * @var array
     */
    protected $languageList;

    /**
     * @var string
     */
    private $lastLanguageUsed;

    /**
     * @var FactoryDefault
     */
    private $di;

    /**
     * @param mixed $languageList
     */
    public function setLanguageList(array $languageList)
    {
        $this->languageList = $languageList;
    }

    /**
     * @return string
     */
    public function getLanguageCode(): string
    {
        return $this->languageCode;
    }

    /**
     * @param string $languageCode
     */
    public function setLanguageCode(string $languageCode)
    {
        $this->languageCode = $languageCode;
    }

    /**
     * Locale constructor.
     */
    public function __construct(FactoryDefault $di)
    {
        $this->di = $di;
        $request = $this->di->get('request');
        /**
         * @var $request Request
         */
        $this->languageCode = $request->getBestLanguage();
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getLanguageList()
    {
        if($this->languageList && $this->lastLanguageUsed == $this->languageCode) {
            return $this->languageList;
        }

        if(is_null($this->languageCode))
        {
            throw new Exception("No language code set");
        }

        $languageList = include BASE_PATH . '/vendor/umpirsky/language-list/data/' . str_replace('-', '_', $this->languageCode) . '/language.php';

        $languageList = array_flip($languageList);

        $languageList = array_map(function($item){
            $item = str_replace('_', '-', $item);
            return $item;
        }, $languageList);

        $languageList = array_flip($languageList);

        $this->lastLanguageUsed = $this->languageCode;
        return $this->languageList = $languageList;
    }

    public function getLanguage()
    {
        return $this->getPrimaryLanguage($this->languageCode);
    }

    public function getLanguageName()
    {
        return $this->getDisplayLanguage($this->languageCode);
    }
}