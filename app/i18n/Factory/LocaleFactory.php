<?php
/**
 * Created by PhpStorm.
 * User: Etshy
 * Date: 18/11/2017
 * Time: 23:49
 */

namespace App\i18n\Factory;


use App\i18n\Locale;
use App\lib\Factory\FactoryInterface;
use Phalcon\Di\FactoryDefault;

class LocaleFactory implements FactoryInterface
{

    /**
     * LocaleFactory constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param FactoryDefault $di
     * @return Locale
     */
    public function createInstance(FactoryDefault $di)
    {

        return new Locale($di);
    }

}