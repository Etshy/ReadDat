ReadDat (temp name)
=====

Installation
----

- Install Phalcon latest 3.X verison (3.3.0 actually) : [Phalcon installation](https://docs.phalconphp.com/en/3.2/installation)
- Download the app (archive or via git cli)
- Change the `config` file according to your installation folder in your web server.
- basic database structure/data are in schema/basic_install.sql
- install the semantic framework https://semantic-ui.com/introduction/getting-started.html 
```
cd public/semantic/
gulp build
```
It will compile the semantic sources already present to generate min files in the dist folder (that iare used)

there is a default admin user with login and password "admin" (recommended to delete it after you register your own user and change him to admin role)
(Users Management will come soon)

Development to do
----

- Finish public pages for teams
- Finish public pages for series
- Users mangaement as Admin (list, view, edit infos/roles)
- Add the settings (default bg, default public footer sentence, default reader mode, etc.)

- Add a install action (?)

- Add a Config class to have a better implementation of the config (change the config call through the whole app needed)
- Config class must have a getter for all keys in application config file (database and mail are apart of the rest of the config)


- When most of the works will be done, look to add AWS and Google cloud integration to store images on cloud.